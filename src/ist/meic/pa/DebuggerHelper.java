package ist.meic.pa;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class DebuggerHelper {
  public static Object processMethodCall(String className, String methodName,
      Object _this, Object[] args, Class<?>[] params) throws Exception {
    Object o = null;
    try {
      Class<?> c = Class.forName(className);
      Method m = c.getDeclaredMethod(methodName, params);
      m.setAccessible(true);
      o = m.invoke(_this, args);
      if(DebugStack.getException()!=null)
        throw DebugStack.getException();
    } catch (Exception ex) {
      System.out.println(ex.getCause());
      DebuggerCLI cli = new DebuggerCLI();
      cli.console();
      // DebugStack.printStack();
      // throw ex;
    }
    return o;
  }

  public static void pushMethodCallHelper(String className, String methodName,
      Object[] args, Object _this) {
    String method = className + "." + methodName + "(";
    for (int i = 0; i < args.length; i++) {
      method += args[i];
      if (i < args.length - 1)
        method += ",";
    }
    method += ")";
    try {
      Class<?> c = Class.forName(className);
      Field[] f = c.getDeclaredFields();
      DebugStack.pushField(f);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    DebugStack.pushMethodCall(method);
    DebugStack.pushCalledObject(_this);
  }
}