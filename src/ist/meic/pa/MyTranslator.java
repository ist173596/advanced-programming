package ist.meic.pa;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.Translator;

public class MyTranslator implements Translator {
  public void start(ClassPool pool) throws NotFoundException,
      CannotCompileException {
    // Do nothing
  }

  public void onLoad(ClassPool pool, String className)
      throws NotFoundException, CannotCompileException {
    CtClass ctClass = pool.get(className);
    myTranslate(ctClass);
  }

  private void myTranslate(CtClass ctClass) throws CannotCompileException {
    if (!ctClass.getPackageName().contains("ist.meic.pa")
        && !ctClass.getPackageName().contains("javassist")) {
      ctClass.instrument(new MyEditor());
    }
  }
}