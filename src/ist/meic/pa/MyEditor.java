package ist.meic.pa;

import javassist.CannotCompileException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

public class MyEditor extends ExprEditor {
  @Override
  public void edit(MethodCall mc) throws CannotCompileException {
    String template = "{"
        + "ist.meic.pa.DebuggerHelper.pushMethodCallHelper(\"%s\", \"%s\", $args, $0);"
        + "$_ = ($r)ist.meic.pa.DebuggerHelper.processMethodCall(\"%s\", \"%s\", $0, $args, $sig);"
        + "ist.meic.pa.DebugStack.popMethodCall();"
        + "ist.meic.pa.DebugStack.popField();"
        + "ist.meic.pa.DebugStack.popCalledObject();"
        + "}";
    String replace = String.format(template, mc.getClassName(), mc.getMethodName(), mc.getClassName(), mc.getMethodName());
    mc.replace(replace);
  }
}