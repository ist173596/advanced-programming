package ist.meic.pa;

import java.util.Scanner;

public class DebuggerCLI {
  private final static Scanner s = new Scanner(System.in);
  
  public void console() {
    while(true) {
      System.out.print("DebuggerCLI:> ");
      s.nextLine();
      DebugStack.printStack();
    }
  }
}