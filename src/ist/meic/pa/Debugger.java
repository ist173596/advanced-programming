package ist.meic.pa;

import java.lang.reflect.Method;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.Loader;
import javassist.NotFoundException;
import javassist.Translator;

public class Debugger {
  public static void main(String[] args) throws NotFoundException,
      CannotCompileException, Throwable {
    if (args.length < 1) {
      System.out.println("To few arguments");
      return;
    }
    Translator translator = new MyTranslator();
    ClassPool pool = ClassPool.getDefault();
    Loader classLoader = new Loader();
    Class<?> c = classLoader.loadClass("ist.meic.pa.DebugStack");
    String[] restArgs = new String[args.length - 1];
    System.arraycopy(args, 1, restArgs, 0, restArgs.length);
    String main = args[0] + ".main(";
    for(int i = 0; i < restArgs.length; i++) {
      main += restArgs[i];
      if(i < restArgs.length - 1) main += ",";
    }
    main += ")";
    Method m = c.getDeclaredMethod("pushMethodCall", String.class);
    m.invoke(null, main);
    classLoader.addTranslator(pool, translator);
    classLoader.run(args[0], restArgs);
  }
}