package ist.meic.pa;

import java.lang.reflect.Field;
import java.util.Stack;

public class DebugStack {
  private static Stack<String> callStack = new Stack<String>();
  private static Stack<Object> calledObject = new Stack<Object>();
  private static Stack<Field[]> field = new Stack<Field[]>();
  private static Exception exception = null;
  
  public static Exception getException(){
    return DebugStack.exception;
  }
  
  public static void setException(Exception e){
    exception = e;
  }
  
  public static void pushMethodCall(String mc) {
    callStack.push(mc);
  }
  
  public static void popMethodCall() {
    callStack.pop();
  }
  
  public static void pushCalledObject(Object co) {
    calledObject.push(co);
  }
  
  public static void popCalledObject() {
    calledObject.pop();
  }
  
  public static void pushField(Field[] f) {
    field.push(f);
  }
  
  public static void popField() {
    field.pop();
  }
  
  private static void printCallStack() {
    System.out.println("Call stack: ");
    for(int i = callStack.size() - 1; i > -1; i--)
      System.out.println(callStack.elementAt(i));
  }
  
  private static void printCalledObject() {
    System.out.print("Called object:");
    System.out.println(calledObject.peek());
  }
  
  private static void printField() {
    Field[] f = field.peek();
    System.out.print("\tField:");
    for(Field ff : f) {
      System.out.print(ff.getName() + " ");
    }
    System.out.println("");
  }
  
  public static void printStack() {
    printCalledObject();
    printField();
    printCallStack();
  }
}