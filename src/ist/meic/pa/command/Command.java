package ist.meic.pa.command;

public interface Command {
  void execute();
  //TODO Abort, Info, Throw, Return, Get, Set, Retry Command Classes
}
